package net.niksune.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.niksune.beans.Client;

public class ServletCreationCommande extends HttpServlet {

	public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/affichageCommande.jsp" ).forward( request, response );
	}

}
