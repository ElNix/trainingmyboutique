package net.niksune.servlets;


	import java.io.IOException;
	import java.io.PrintWriter;

	import javax.servlet.ServletException;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

import net.niksune.beans.Client;

	public class ServletCreationClient extends HttpServlet {

		public void doGet( HttpServletRequest request, HttpServletResponse response )	throws ServletException, IOException {
			
			boolean success = false;
			
			String nom = request.getParameter("nomClient");
			String prenom = request.getParameter("prenomClient");
			String adresse = request.getParameter("adresseClient");
			String telephone = request.getParameter("telephoneClient");
			String email = request.getParameter("emailClient");
			
			Client client = new Client(nom,
					prenom,
					adresse,
					telephone,
					email);
			
			if(nom != "" && adresse != "" && email != "")
				success = true;
			
			request.setAttribute("client", client);
			request.setAttribute("success", success);

			this.getServletContext().getRequestDispatcher( "/WEB-INF/affichageClient.jsp" ).forward( request, response );
		}
}
