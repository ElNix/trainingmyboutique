<%@ page language='java' contentType='text/html; charset=UTF-8'
    pageEncoding='UTF-8'%>
<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<title>Client</title>
</head>
<body>
<p>
	${success ? 'Client créé avec succès !' : 'Problème lors de la saisie client' }
</p>
<p>
	Nom : ${client.nom }<br/>
	Prenom : ${client.prenom }<br/>
	Adresse : ${client.adresse }<br/>
	Telephone : ${client.telephone }<br/>
	Email : ${client.email }<br/>
</p>

</body>
</html>